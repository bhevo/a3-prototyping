using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
    
{
    public Text winTxt;
    public bool gameEnd;
    // Start is called before the first frame update
    void awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

       if (gameEnd)
        {
            EndGame();
        }
    }

    void EndGame()
    {
        TimerController.instance.EndTimer();
        winTxt.text = "You beat the level with a time of : " + TimerController.instance.timeTxt.text;
        //Time.timeScale = 0;

    }
}

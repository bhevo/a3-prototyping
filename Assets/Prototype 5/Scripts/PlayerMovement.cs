﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerMovement : MonoBehaviour
{

   // private Vector3 hitNormal;
    //public float slopeAngle;
    //public bool isGrounded;


    // REFERENCE = ALPHAROAH (YOUTUBE)
    public Text speedTxt;
	public CharacterController controller;
	public Transform GroundCheck;
	public LayerMask GroundMask;
	private float wishspeed2;
	private float gravity = -20f;
	float wishspeed;
	public float groundDistance = 0.4f;
	public float moveSpeed = 7.0f;  
	public float runAcceleration = 14f;   
	public float runDeacceleration = 10f;   
	public float airAcceleration = 2.0f;  
	public float airDeacceleration = 2.0f;    
	public float aircontrol = 0.3f;  
	public float sideStrafeAcceleration = 50f;  
	public float sideStrafeSpeed = 1f;    
	public float jumpHeight = 8.0f;
	public float friction = 6f;
	private float playerTopVelocity = 0;
	public float playerFriction = 0f;
	float addspeed;
	float accelspeed;
	float currentspeed;
	float zspeed;
	float speed;
	float dot;
	float k;
	float accel;
	float newspeed;
	float control;
	float drop;
	public bool JumpQueue = false;
	public bool wishJump = false;
	public Vector3 moveDirection;
	public Vector3 moveDirectionNorm;
	private Vector3 playerVelocity;
	Vector3 wishdir;
	Vector3 vec;
	public Transform playerView;
	public float x;
	public float z;
	public bool IsGrounded;
	public Transform player;
	Vector3 udp;

    public GameManager gameManagerScript;
 
    void Update()
	{
        speedTxt.text = "Speed: " + controller.velocity.magnitude.ToString();
		IsGrounded = Physics.CheckSphere(GroundCheck.position, groundDistance, GroundMask);
        /*
        if (slopeAngle > controller.slopeLimit)
        {
            IsGrounded = false;
            playerVelocity = Vector3.ProjectOnPlane(playerVelocity, hitNormal);
        }*/
        QueueJump();
	
		if (controller.isGrounded)
			GroundMove();
		else if (!controller.isGrounded)
			AirMove();
		controller.Move(playerVelocity * Time.deltaTime);
		
		udp = playerVelocity;
		udp.y = 0;
		if (udp.magnitude > playerTopVelocity)
			playerTopVelocity = udp.magnitude;
        
        
        
	}

   /* private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        hitNormal = hit.normal;
        slopeAngle = Mathf.Round(Vector3.Angle(Vector3.up, hitNormal) * 100) / 100;
    }*/

    public void AirMove()
    {
        SetMovementDir();

        wishdir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        wishdir = transform.TransformDirection(wishdir);

        wishspeed = wishdir.magnitude;

        wishspeed *= 7f;

        wishdir.Normalize();
        moveDirectionNorm = wishdir;


        wishspeed2 = wishspeed;
        if (Vector3.Dot(playerVelocity, wishdir) < 0)
            accel = airDeacceleration;
        else
            accel = airAcceleration;


        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") != 0)
        {
            if (wishspeed > sideStrafeSpeed)
                wishspeed = sideStrafeSpeed;
            accel = sideStrafeAcceleration;
        }

        Accelerate(wishdir, wishspeed, accel);

        AirControl(wishdir, wishspeed2);
        playerVelocity.y += gravity * Time.deltaTime;
        void AirControl(Vector3 wishdir, float wishspeed)
        {
            if (Input.GetAxis("Horizontal") == 0 || wishspeed == 0)
                return;

            zspeed = playerVelocity.y;
            playerVelocity.y = 0;
            speed = playerVelocity.magnitude;
            playerVelocity.Normalize();
            dot = Vector3.Dot(playerVelocity, wishdir);
            k = 32;
            k *= aircontrol * dot * dot * Time.deltaTime;

            if (dot > 0)
            {
                playerVelocity.x = playerVelocity.x * speed + wishdir.x * k;
                playerVelocity.y = playerVelocity.y * speed + wishdir.y * k;
                playerVelocity.z = playerVelocity.z * speed + wishdir.z * k;

                playerVelocity.Normalize();
                moveDirectionNorm = playerVelocity;
            }

            playerVelocity.x *= speed;
            playerVelocity.y = zspeed;
            playerVelocity.z *= speed;

        }
    }

    void QueueJump()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded)
        {
            wishJump = true;
        }

        if (!IsGrounded && Input.GetButtonDown("Jump"))
        {
            JumpQueue = true;
        }
        if (IsGrounded && JumpQueue)
        {
            wishJump = true;
            JumpQueue = false;
        }
    }
    public void SetMovementDir()
	{
		x = Input.GetAxis("Horizontal");
		z = Input.GetAxis("Vertical");
	}

	
	

	//Calculates acceleration
	public void Accelerate(Vector3 wishdir, float wishspeed, float accel)
	{
		currentspeed = Vector3.Dot(playerVelocity, wishdir);
		addspeed = wishspeed - currentspeed;
		if (addspeed <= 0)
			return;
		accelspeed = accel * Time.deltaTime * wishspeed;
		if (accelspeed > addspeed)
			accelspeed = addspeed;

		playerVelocity.x += accelspeed * wishdir.x;
		playerVelocity.z += accelspeed * wishdir.z;
	}

	

	
	public void GroundMove()
	{
		
		if (!wishJump)
			ApplyFriction(1.0f);
		else
			ApplyFriction(0);

		SetMovementDir();

		wishdir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		wishdir = transform.TransformDirection(wishdir);
		wishdir.Normalize();
		moveDirectionNorm = wishdir;

		wishspeed = wishdir.magnitude;
		wishspeed *= moveSpeed;

		Accelerate(wishdir, wishspeed, runAcceleration);

		
		playerVelocity.y = 0;

		if (wishJump)
		{
			playerVelocity.y = jumpHeight;
			wishJump = false;
		}

		
		void ApplyFriction(float t)
		{
			vec = playerVelocity; 
			vec.y = 0f;
			speed = vec.magnitude;
			drop = 0f;

			
			if (controller.isGrounded)
			{
				control = speed < runDeacceleration ? runDeacceleration : speed;
				drop = control * friction * Time.deltaTime * t;
			}

			newspeed = speed - drop;
			playerFriction = newspeed;
			if (newspeed < 0)
				newspeed = 0;
			if (speed > 0)
				newspeed /= speed;

			playerVelocity.x *= newspeed;
			playerVelocity.z *= newspeed;
		}


        
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("jumpBoost"))
        {
            //Debug.Log("JumpBoost");
            jumpHeight = jumpHeight + 5;

        }
        
        if (other.CompareTag("Ground"))
        {
            jumpHeight = 8;
			moveSpeed = 8;
            
        }

		if (other.CompareTag("slowDebuff"))
        {
			moveSpeed = moveSpeed - 4;
        }

        if (other.CompareTag("speedBuff"))
        {
            moveSpeed = moveSpeed + 20;
        }

        if (other.CompareTag("gameEnd"))
        {
            gameManagerScript.gameEnd = true;
        }
    }
}

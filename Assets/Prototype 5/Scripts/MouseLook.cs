using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseLook : MonoBehaviour
{
    public float Sensitivity = 100f;
    public Transform playerBody;
    public Text sensTxt;

    private float xRotation = 0f;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * Sensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * Sensitivity * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.F))
        {
            Sensitivity = Sensitivity + 100;
            Debug.Log("Increased Sensitivity By 100");
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            Sensitivity = Sensitivity - 100;
            Debug.Log("Increased Sensitivity By 100");
        }

        playerBody.Rotate(Vector3.up * mouseX);

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        sensTxt.text = "Sensitivity: " + Sensitivity.ToString();
    }
}
